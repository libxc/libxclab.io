+++
type = "page"
title = "Change Log"
description = "Complete list of changes made to the library over the different releases"
weight = 10
pre = "<i class='fa fa-list-alt'>&nbsp;</i> " 
+++

{{% content "changes/ChangeLog.md" %}}

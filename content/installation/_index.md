+++
head ="<label>Documentation</label> "
title = "Installation"
weight = 30
pre = "<i class='fa fa-cogs'>&nbsp;</i> " 
+++

# Autotools

If you are installing the library directly from the sources, the recommended way to do so is by using GNU Autotools.

To install the library, just use the standard procedure:
```
./configure --prefix=PATH/TO/LIBXC
make
make check
make install
```
Do not forget to replace `PATH/TO/LIBXC` by the some appropriate path on your file system and make sure that you have write permissions.

If you obtained the sources directly from the git repository, you will first need to generate `configure` by running
```
autoreconf -i
```


# CMake

Support for building the sources using CMake has also been recently contributed by Lori Burns.

The CMake file has the following caveats

* tested on Linux and Mac, static and shared lib, namespaced and non-namespaced headers, but really only to the extent that it works for Psi4
* all the fancy libtool options and Fortran interface _not_ tested
* test suite executed after build via `ctest`. But it has always totally passed or totally failed, which doesn't inspire confidence
* The generated `libxc_docs.txt` is large, and the generation step sometimes balks on it, leading to `xc_funcs.h` not found errors. Just execute again.

## Building with CMake

Use the following procedure:

```bash
cmake -H. -Bobjdir
cd objdir && make
make test
make install
```

The build is also responsive to

* static/shared toggle `BUILD_SHARED_LIBS`
* install location `CMAKE_INSTALL_PREFIX`
* namespacing of headers `NAMESPACE_INSTALL_INCLUDEDIR`
* of course, `CMAKE_C_COMPILER`, `BUILD_TESTING`, and `CMAKE_C_FLAGS`

See [CMakeLists.txt](CMakeLists.txt) for options details. All these build options should be passed as `cmake -DOPTION`.
## Detecting with CMake

CMake builds install with `LibxcConfig.cmake`, `LibxcConfigVersion.cmake`, and `LibxcTargets.cmake` files suitable for use with CMake [`find_package()`](https://cmake.org/cmake/help/v3.2/command/find_package.html) in `CONFIG` mode.

* `find_package(Libxc)` - find any xc libraries and headers
* `find_package(Libxc 3.0.0 EXACT CONFIG REQUIRED COMPONENTS static)` - find Libxc exactly version 3.0.0 built with static libraries or die trying

See [cmake/LibxcConfig.cmake.in](cmake/LibxcConfig.cmake.in) for details of how to detect the Config file and what CMake variables and targets are exported to your project.

## Use with CMake

After `find_package(Libxc ...)`,

* test if package found with `if(${Libxc_FOUND})` or `if(TARGET Libxc::xc)`
* link to library (establishes dependency), including header and definitions configuration with `target_link_libraries(mytarget Libxc::xc)`
* include header files using `target_include_directories(mytarget PRIVATE $<TARGET_PROPERTY:Libxc::xc,INTERFACE_INCLUDE_DIRECTORIES>)`
* compile target applying `-DUSING_Libxc` definition using `target_compile_definitions(mytarget PRIVATE $<TARGET_PROPERTY:Libxc::xc,INTERFACE_COMPILE_DEFINITIONS>)`


## Python Library

Optional Python bindings are available through the cytpes module. To install
into Python site-packages plese run:
```
python setup.py install
```
or, to install locally for development:
```
python setup.py develop
```

The Python bindings require the CMake compilation pathway and the Python
Numerical Python library. A short usage example is provided below:
```python
# Build functional
>>> func = pylibxc.LibXCFunctional("gga_c_pbe", "unpolarized")

# Create input
>>> inp = {}
>>> inp["rho"] = np.random.random((3))
>>> inp["sigma"] = np.random.random((3))

# Compute
>>> ret = func.compute(inp)
>>> for k, v in ret.items():
>>>     print(k, v)

zk [[-0.06782171 -0.05452743 -0.04663709]]
vrho [[-0.08349967 -0.0824188  -0.08054892]]
vsigma [[ 0.00381277  0.00899967  0.01460601]]
```

# Fedora Linux

Libxc is available in Fedora Linux. To compile against Libxc, one just needs to install the development package with
```bash
 $ dnf -y install libxc-devel
```

If the Libxc Fortran module file is needed, one needs to add '-I/usr/lib64/gfortran/modules' (on 64-bit systems) or '-I/usr/lib/gfortran/modules' (on 32-bit systems) to the compiler arguments.

# Homebrew

Libxc for Mac OS X is available from the Homebrew package manager. After Homebrew installation one types in:
```bash
$ brew install gfortran
$ brew install libxc
```

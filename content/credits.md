+++
title = "Credits"
description = "contributors to libxc"
+++

#### Main authors
 * Miguel A. L. Marques (marques@tddft.org)
 * Micael Oliveira
 * Susi Lehtola

#### Other contributors
 * Tobias Burnus
 * Georg Madsen (XC_GGA_X_PBEA)
 * Xavier Andrade
 * David Strubbe
 * Lori A. Burns (CMake build system)
 * Daniel Smith (Python interface)
